
// TEXT WRITING!

var i = -20;
var txt = 'A candy-sweet habit tracker'; /* The text */
var speed = 50; /* The speed/duration of the effect in milliseconds */

function typeWriter() {
  if (i < txt.length) {
    document.getElementById("subtitle").innerHTML += txt.charAt(i);
    i++;
    setTimeout(typeWriter, speed);
  }

}

// windowsheight
$(document).ready(function() {
  function setHeight() {
    windowHeight = $(window).innerHeight();
    $('.win-height').css('min-height', 1.1 * windowHeight);
  };
  setHeight();
  
  $(window).resize(function() {
    setHeight();
  });
});

// EXECUTE:
var  g = document.getElementsByTagName('body')[0];

typeWriter();


